# -*- coding: utf-8 -*-
"""
Created on Wed Jan 23 11:48:16 2019

@author: pkovacs-lt
"""

import random
import itertools
import time

SUIT = ('H', 'D', 'C', 'S')
RANK = [i+1 for i in range(13)]


class Card:
    
    def __init__(self, suit, rank):
        self.suit = suit
        self.rank = rank
        
    def __str__(self):
        return str(self.rank) + self.suit
    
    def value(self):
        if self.rank == 1:
            return 11
        if self.rank < 10:
            return self.rank
        else:
            return 10
  
    
class Deck:
    
    def __init__(self):
        self.deck = [Card(i[0], i[1]) for i in itertools.product(SUIT,RANK)]
        
    def shuffle(self):
        random.shuffle(self.deck)
    
    def __str__(self):
        deck = ''
        for card in self.deck:
            deck += card.__str__() + ' '
        return deck

    def deal(self):
        return self.deck.pop()
   
    
class Hand:
    
    def __init__(self):
        self.cards = []
        self.value = 0
        self.aces = 0
    
    def hit(self, deck):
        card = deck.deal()
        self.cards.append(card)
        self.value += card.value()
        if card.rank == 1:
            self.aces += 1
        if self.aces:
            self.ace_adjust()
            
    def ace_adjust(self):
        while self.value > 21 and self.aces:    
            self.value -= 10
            self.aces -= 1

    def __str__(self):
        hand = ''
        for card in self.cards:
            hand += card.__str__() + ' '
        return hand
    
    def __getitem__(self, idx):
        return self.cards[idx]
    
class Chips:
    
    def __init__(self, total=100):
        self.total = total
        self.bet = 0
    
    def win(self):
        self.total += self.bet
    
    def lose(self):
        self.total -= self.bet
        
    def __str__(self):
        return f"You have {self.total} chips.\n"
    
class Player:
    
    def __init__(self, hand, chips):
        self.hand = hand
        self.chips = chips


def take_bet(chips):
    while True:
        try:
            bet = int(input('How much to bet?'))
            chips.bet = bet
        except ValueError:
            print("Sorry, your bet must be an integer!")
        else:
            if bet > chips.total:
                print(f"You can't bet more than {chips.total}!")
            else:
                break


def hit(deck, hand):
    hand.hit(deck)

def hit_or_stand(deck, hand):
    
    global playing
    while True:
        action = input('(H)it or (S)tand?')
    
        if action[0].upper() == 'H':
            hit(deck, hand)
            break
        elif action[0].upper() == 'S':
            playing = False
            break
        else:
            print('Sorry, select Hit or Stand.')
            

def show_some(player, dealer):
    cards = ''
    print('Dealer showing:')
    for card in dealer[1:]:
        cards += card.__str__()    
    print(card)

    cards = ''
    print(f'Player showing: {player.value}' )
    print(player)
    print('\n')


def show_one(player, dealer):
    print(f'Dealer showing: {dealer[0].value()}')
    print(dealer[0].__str__() + ' --')
    print(f'Player showing: {player.value}')
    print(player)
    print('\n')
    
    
def show_all(player, dealer):
    print(f'Dealer showing: {dealer.value}')
    print(dealer)
    print(f'Player showing: {player.value}')
    print(player)
    print('\n')


def player_busts(player, chips):
    print(f'Player busts showing {player.value}')
    print('Dealer Wins!')
    chips.lose()
    print(chips)


def player_wins(chips):
    print('Player wins!')
    chips.win()
    print(chips)


def dealer_busts(dealer, chips):
    print(f'Dealer busts showing {dealer.value}')
    print('Player Wins!')
    chips.win()
    print(chips)

def dealer_wins(chips):
    print('Dealer wins!')
    chips.lose()
    print(chips)


def push(chips):
    print("It's a push!")
    print(chips)

if __name__=="__main__":
    print('Welcome to the table, take a seat.')
    player_chips = Chips()
    while True:
        print('Shuffling!')
        d = Deck()
        d.shuffle()
        player_hand = Hand()
        dealer_hand = Hand()
        
        
        take_bet(player_chips)
        
        for i in range(2):
            player_hand.hit(d)
            dealer_hand.hit(d)
        
        playing = True
        print('Players turn: \n')
        show_one(player_hand, dealer_hand)
        while playing:
            hit_or_stand(d, player_hand)
            show_one(player_hand, dealer_hand)
            if player_hand.value > 21:
                player_busts(player_hand, player_chips)
                break
        
        if playing == False:
            show_all(player_hand, dealer_hand)  
            while (dealer_hand.value < 17):
                time.sleep(1)
                dealer_hand.hit(d)
                print('Dealer hits')
                show_all(player_hand, dealer_hand)
                
            print(f'Player has {player_hand.value}')
            print(f'Dealer has {dealer_hand.value}')  
            
            if dealer_hand.value > 21:
                dealer_busts(dealer_hand, player_chips)
                
            elif dealer_hand.value == player_hand.value:
                push(player_chips)
                
            elif dealer_hand.value > player_hand.value:
                dealer_wins(player_chips)
            
            else:
                player_wins(player_chips)
                
            
            play_again = input('Would you like to play again?')
            
            if play_again[0].lower() == 'n':
                break
        
        
        
        
        

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    