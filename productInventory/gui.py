import wx
import wx.lib.inspection
from main import *

class MainWindow(wx.Frame):
    def __init__(self, parent, title):
        wx.Frame.__init__(self, parent, title=title, size=(1000,750))
        self.control = wx.TextCtrl(self, style=wx.TE_MULTILINE)
        self.CreateStatusBar() # A Statusbar in the bottom of the window

        textarea = wx.TextCtrl(self, -1, style=wx.TE_READONLY, size=(200,100))
        self.usertext = textarea
        # Setting up the menu.
        filemenu= wx.Menu()

        # wx.ID_ABOUT and wx.ID_EXIT are standard IDs provided by wxWidgets.
        aboutItem = filemenu.Append(wx.ID_ABOUT, "&About"," Information about this program")
        filemenu.AppendSeparator()
        exitItem = filemenu.Append(wx.ID_EXIT,"Exit"," Terminate the program")

        # Creating the menubar.
        menuBar = wx.MenuBar()
        menuBar.Append(filemenu,"&File") # Adding the "filemenu" to the MenuBar
        self.SetMenuBar(menuBar)  # Adding the MenuBar to the Frame content.

        
        self.sizer_h = wx.BoxSizer(wx.HORIZONTAL)
        self.sizer_v = wx.BoxSizer(wx.VERTICAL)
        self.buttons = []
        
        # add buttons
        add_button = wx.Button(self, -1, 'Add')
        sub_button = wx.Button(self, -1, 'Subtract')
        gen_button = wx.Button(self, -1, 'Generate')

        #self.buttons.append(wx.Button(self, -1, 'Add'))
        #self.buttons.append(wx.Button(self, -1, 'Subtract'))
        #gen_button = self.buttons.append(wx.Button(self, 1, 'Generate Random'))

        self.buttons.append(add_button)
        self.buttons.append(sub_button)
        self.buttons.append(gen_button)

        for button in self.buttons:
        	self.sizer_h.Add(button, 1, wx.EXPAND)

		# Even Handling
        self.Bind(wx.EVT_MENU, self.OnAbout, aboutItem)
        self.Bind(wx.EVT_MENU, self.OnExit, exitItem)
        self.Bind(wx.EVT_BUTTON, self.OnGen, gen_button)


        # Use some sizers to see layout options
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        
        self.sizer.Add(self.control, 1, wx.EXPAND)
        self.sizer.Add(self.sizer_h, 0, wx.EXPAND)

        #Layout sizers
        self.SetSizer(self.sizer)
        self.SetAutoLayout(1)
		
        self.Show(True)

    def OnAbout(self, event): 
        # Create a pop-up message dialog box
        print("OnAbout")
        dlg = wx.MessageDialog(self, "Inventory Tracking System", "Inventory Manager", wx.OK)
        dlg.ShowModal() # Shows it
        dlg.Destroy() # finally destroy it when finished.

    def OnExit(self, event):
    	self.Close(True)

    def OnGen(self, event):
    	plist = [Product(randint(1,100), i, randint(20,30))for i in range(1,100)]
    	inv = Inventory(plist)
    	print(inv)
    	self.usertext.AppendText(inv.__str__())
    	#print("I pressed the button!")

app = wx.App(False)
frame = MainWindow(None, "Inventory Manager")
#wx.lib.inspection.InspectionTool().Show()
app.MainLoop()