# -*- coding: utf-8 -*-
"""
@author: pkovacs-lt
"""

from tabulate import tabulate
from random import randint

class Product:

    def __init__(self, price, id, count):
        self.price = price
        self.id = id
        self.count = count

    def total_value(self):
        return self.price*self.count


class Inventory:

    def __init__(self, products=None):    
        if products==None:
            products = []
        elif not isinstance(products, list):
            products = [products]

        self.products=products

    def add_product(price,id,count):
        self.products.append(Product(price, id, count))


    def __str__(self):
        data = [[prod.id, prod.price, prod.count, prod.total_value()]for prod in self.products]
        return tabulate(data, headers=['id','Price','Count','Value'])



if __name__ =="__main__":
    plist = [Product(randint(1,100), i, randint(20,30))for i in range(1,100)]

    inv = Inventory(plist)

    print(inv)

